class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        len_1 = len(nums1)
        len_2 = len(nums2)
        
        len_tot = len_1 + len_2
        len_diff = abs(len_1-len_2)
        
        k1=0
        k2=0
        s=0
        i = int(len_tot/2.0)
        count=0
        while k1 < len_1 or k2 < len_2:
            if k1 < len_1 and k2 < len_2:
                if nums1[k1]<nums2[k2]:
                    s = nums1[k1]
                    k1+=1               
                elif nums1[k1]>nums2[k2]:
                    s = nums2[k2]
                    k2+=1    
                elif nums1[k1]==nums2[k2]:
                    s = nums1[k1]
                    k1+=1
                    #k2+=1
                    
            elif  k1 == len_1 and k2 < len_2:
                s = nums2[k2]
                k2+=1
            elif k2 == len_2 and k1 < len_1:
                s = nums1[k1]
                k1+=1
            count+=1
            
            if count==i:
                s1 = s
            if count==i+1:
                s2=s
            
            #print(s, count)
        #print(s1,s2)
        
        if (len_tot % 2)==0:
            return (s1+s2)/2.0
        else:
            return float(s2)
