class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if numRows>=len(s) or numRows==1:
            return s
        else:
        
            s0=''
            i=0
            j=1
            while True:
                try:
                    s0+=s[i*(numRows-1)]
                    i+=2
                except:
                    break
                       
            k=1
            while k < numRows-1:
                s0+=s[k]
                i=2
                while True:
                    try:
                        s0+=s[i*(numRows-1)-k]
                        s0+=s[i*(numRows-1)+k]
                        i+=2
                    except:
                        k+=1
                        break
        
            while True:
                try:
                    s0+=s[j*(numRows-1)]
                    j+=2
                except:
                    break
        
            return s0
