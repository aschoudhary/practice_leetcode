class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        ss = ""
        k=0
        l = 0
        while True:
            try:
                if s[k] not in ss:
                    ss+=s[k]
                    k+=1 
            
                elif s[k] in ss:
                    ss = ss[ss.index(s[k])+1:]
                    ss+=s[k]
                    k+=1
                if len(ss)>l:
                    l = len(ss)  
        
                print(ss)  
                    
            except:
                break

        return l
