# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):    
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        b = {}
        k=0
        sm = l1.val + l2.val
        carry = 0
        if sm >=10:
            sm = sm -10
            carry = 1
        b[k] = ListNode(sm)
        
        while True:
            try:
                k+=1
                sm = l1.next.val + l2.next.val + carry
                if sm >=10:
                    sm = sm -10
                    carry = 1
                else:
                    carry=0
                l1 = l1.next
                l2 = l2.next
                b[k] = ListNode(sm)
                b[k-1].next = b[k]
            except:
                break
        while True:
            try:
                sm = l1.next.val+carry
                if sm >=10:
                    sm = sm -10
                    carry = 1
                else:
                    carry=0
                l1 = l1.next
                b[k] = ListNode(sm)
                b[k-1].next = b[k]
                k+=1
            except:
                break
        while True:
            try:
                sm = l2.next.val+carry
                if sm >=10:
                    sm = sm -10
                    carry = 1
                else:
                    carry=0
                l2 = l2.next
                b[k] = ListNode(sm)
                b[k-1].next = b[k]
                k+=1
            except:
                break
        if carry==1:
            b[k] = ListNode(1)
            b[k-1].next = b[k]
            k+=1 
                
        return b[0]

